﻿namespace SampleApi.Service.Implementation
{
    using Microsoft.AspNetCore.Mvc;
    using SampleApi.Core;
    using SampleApi.Models;
    using SampleApi.Service.Interfaces;
    using System;
    using System.Threading.Tasks;

    public class FizzBuzzService : IFizzBuzzService
    {
        private readonly EntityContext _context;

        public FizzBuzzService(EntityContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Modulo35(ModuloModel model, string ip)
        {
            try
            {
                var item = new ModuloResult();

                if (model.Number % 3 == 0 && model.Number % 5 == 0)
                {
                    item.Answer = "fizzbuzz";
                }
                else if (model.Number % 3 == 0)
                {
                    item.Answer = "fizz";
                }
                else if (model.Number % 5 == 0)
                {
                    item.Answer = "buzz";
                }
                else
                {
                    _context.Add(new Report
                    {
                        Request = model.ToString(),
                        Response = model.ToString(),
                        IP = ip
                    });
                    await _context.SaveChangesAsync();

                    return new OkObjectResult(model);
                }

                await _context.Reports.AddAsync(new Report
                {
                    Request = model.Number.ToString(),
                    Response = item.Answer,
                    IP = ip
                });
                await _context.SaveChangesAsync();

                return new OkObjectResult(item);
            }
            catch (Exception ex)
            {
                _context.Reports.Add(new Report
                {
                    Request = model.Number.ToString(),
                    Response = ex.Message,
                    IP = ip
                });
                await _context.SaveChangesAsync();

                return new BadRequestObjectResult(new ErrorModel { Error = ex.Message });
            }
        }

        public async Task<IActionResult> Modulo735(ModuloModel model, string ip)
        {
            try
            {
                var item = new ModuloResult();
                if (model.Number % 3 == 0 && model.Number % 5 == 0 && model.Number % 7 == 0)
                {
                    item.Answer = "fizzbuzzwizz";
                }
                else if (model.Number % 5 == 0 && model.Number % 7 == 0)
                {
                    item.Answer = "buzzwizz";
                }
                else if (model.Number % 3 == 0 && model.Number % 7 == 0)
                {
                    item.Answer = "fizzbuzz";
                }
                else if (model.Number % 7 == 0)
                {
                    item.Answer = "wizz";
                }
                else
                {
                    _context.Reports.Add(new Report
                    {
                        Request = model.Number.ToString(),
                        Response = item.Answer,
                        IP = ip
                    });
                    await _context.SaveChangesAsync();

                    return new OkObjectResult(model);
                }

                _context.Reports.Add(new Report
                {
                    Request = model.Number.ToString(),
                    Response = item.Answer,
                    IP = ip
                });
                await _context.SaveChangesAsync();

                return new OkObjectResult(item);
            }
            catch (Exception ex)
            {
                _context.Reports.Add(new Report
                {
                    Request = model.Number.ToString(),
                    Response = ex.Message,
                    IP = ip
                });

                await _context.SaveChangesAsync();

                return new BadRequestObjectResult(new ErrorModel { Error = ex.Message });
            }
        }
    }
}
