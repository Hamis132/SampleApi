﻿namespace SampleApi.Service.Interfaces
{
    using Microsoft.AspNetCore.Mvc;
    using SampleApi.Models;
    using System.Threading.Tasks;

    public interface IFizzBuzzService
    {
        Task<IActionResult> Modulo735(ModuloModel modulo, string ip);
        Task<IActionResult> Modulo35(ModuloModel modulo, string ip);
    }
}
