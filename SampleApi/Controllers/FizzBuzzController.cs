﻿namespace SampleApi.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using SampleApi.Models;
    using SampleApi.Service.Interfaces;
    using System.Threading.Tasks;

    public class FizzBuzzController : Controller
    {
        private readonly IFizzBuzzService _fizzbuzz;
        public FizzBuzzController(IFizzBuzzService fizzbuzz)
        {
            _fizzbuzz = fizzbuzz;
        }

        [HttpPost("/api/modulo35")]
        public async Task<IActionResult> ModuloResult35([FromBody] ModuloModel model)
        {
            var ip = HttpContext.Connection.RemoteIpAddress;

            return await _fizzbuzz.Modulo35(model, ip.MapToIPv4().ToString());
        }

        [HttpPost("/api/modulo357")]
        public async Task<IActionResult> ModuloResult357([FromBody] ModuloModel model)
        {
            var ip = HttpContext.Connection.RemoteIpAddress;

            return await _fizzbuzz.Modulo735(model, ip.MapToIPv4().ToString());
        }
    }
}
