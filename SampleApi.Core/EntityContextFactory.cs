﻿namespace SampleApi.Core
{
    using Microsoft.EntityFrameworkCore;
    using SampleApi.Core.Infrastructure;

    public class EntityContextFactory : DesignTimeDbContextFactoryBase<EntityContext>
    {
        protected override EntityContext CreateNewInstance(DbContextOptions<EntityContext> options)
        {
            return new EntityContext(options);
        }
    }
}
