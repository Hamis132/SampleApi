﻿namespace SampleApi.Core
{
    public class Report
    {
        public int Id { get; set; }
        public string IP { get; set; }
        public string Request { get; set; }
        public string Response { get; set; }
    }
}
